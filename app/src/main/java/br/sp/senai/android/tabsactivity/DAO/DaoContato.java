package br.sp.senai.android.tabsactivity.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.sp.senai.android.tabsactivity.Model.Contato;


public class DaoContato extends SQLiteOpenHelper {
    private SQLiteDatabase banco;
    private static int versao = 1;
    private final String SQL_INCLUIR =
            "insert into contato (nome, documento) values (?, ?)";

    public DaoContato(Context context) {
        super(context, "contato", null, versao);
    }

    @Override
    // Criar o banco de dados
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists contato (" +
                "id integer primary key autoincrement, " +
                "nome text not null, " + "documento text not null);");
    }

    @Override
    // Atualizar o banco de dados
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("Glauber", "Atualizando o database");
        db.execSQL("drop table if exists contato;");
        db.execSQL("create table if not exists contato (id integer not null autoincrement, " +
                "nome text not null, " + "documento text not null);");
    }

    public void incluir(Contato c) {
        ContentValues colunasDoBanco = new ContentValues();
        colunasDoBanco.put("nome", c.getNome());
        colunasDoBanco.put("documento", c.getDocumento());

        banco = getWritableDatabase();
        c.setId((int) banco.insert("contato", null, colunasDoBanco));
        banco.close();
    }

    public List<Contato> listar() {
        List<Contato> listagem = new ArrayList<>();

        // Programar aqui
        String[] colunas = new String[]{"id", "nome", "documento"};

        banco = getWritableDatabase();
        Cursor dados = banco.query("contato", colunas, null, null, null, null, null);
        dados.moveToFirst();

        while (dados.moveToNext()) {

            Contato c = new Contato();
            c.setId((int) dados.getLong(0));
            c.setNome(dados.getString(1));
            c.setDocumento(dados.getString(2));

            listagem.add(c);
        }

        dados.close();
        return listagem;
    }

    public void deletar(Contato contato) {
        banco = getWritableDatabase();

        long id = contato.getId();
        banco.delete("contato","id" + " = " + id, null);

    }
}
