package br.sp.senai.android.tabsactivity.Model;

import java.util.List;

/**
 * Created by Tecnico_Tarde on 14/10/2016.
 */
public class Contato {
    private int id;
    private String nome;
    private String documento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Nome: " + nome + ", " + "Documento: " + documento;
    }
}
