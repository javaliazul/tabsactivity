package br.sp.senai.android.tabsactivity.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.sp.senai.android.tabsactivity.Model.Contato;
import br.sp.senai.android.tabsactivity.R;

/**
 * Created by Tecnico_Tarde on 14/10/2016.
 */
public class ContatoAdapter extends BaseAdapter {
    private List<Contato> contatos;
    private LayoutInflater renderizador;
//    private Context context;

    public ContatoAdapter(Context context, List<Contato> contatos) {
        this.contatos = contatos;
        renderizador = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return contatos.size();
    }

    @Override
    public Contato getItem(int position) {
        return contatos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contato contato = contatos.get(position);

        if (convertView == null)
            convertView = renderizador.inflate(R.layout.listacontatos_activity, null);
//            convertView = LayoutInflater.from(context).inflate(R.layout.atividade_layout, null);

        TextView nomeContato = (TextView) convertView.findViewById(R.id.nome_contato);
        nomeContato.setText(contato.getNome());


        return convertView;
    }

}
